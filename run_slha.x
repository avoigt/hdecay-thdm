#!/bin/sh

if test $# -lt 1
then
    cat <<EOF
Usage: $0 <slha-file>
EOF
    exit 1
fi

slha_file="$1"
hdecay_file="hdecay.in"
template="hdecay.tmpl"

# extracs a block from a SLHA file
get_block="\
BEGIN {
   is_block = 0
   if (block == \"\") {
      print \"Error: block name not defined\"
      print \"   Please define the block name with -v block=<block-name>\"
      exit 1
   }
}
{
   # SLHA defines that the first character of a block statement is
   # 'B'.  However, for compatibility we allow for 'b' as well.

   pattern     = \"^block[[:blank:]]*\" tolower(block) \"([^[:graph:]].*)?\$\"
   not_pattern = \"^block[[:blank:]]*.*\$\"

   # skip comments
   if (\$1 ~ \"^#\") next

   if (tolower(\$0) ~ pattern) {
      is_block = 1
   } else if (tolower(\$0) ~ not_pattern) {
      is_block = 0
   }

   if (is_block)
      print \$0
}
"

# extract an entry from a SLHA block
get_entry="\
{
   if (\$1 == entry)
      print \$2
}
"

# extract an entry with two indices from a SLHA block
get_entry2="\
{
   if (\$1 == entry1 && \$2 == entry2)
      print \$3
}
"

arccos()
{
    perl -E "use Math::Trig; say acos($1)"
}

prod()
{
    perl -E "say $1*$2"
}

tofortran()
{
    value=$(perl -E "say $1")
    echo "${value}D0"
}

mass_block=$(awk -e "$get_block" -v block=MASS $slha_file)
mh=$(echo "$mass_block" | awk -e "$get_entry" -v entry=25)
mH=$(echo "$mass_block" | awk -e "$get_entry" -v entry=35)
mA=$(echo "$mass_block" | awk -e "$get_entry" -v entry=36)
mC=$(echo "$mass_block" | awk -e "$get_entry" -v entry=37)

minpar_block=$(awk -e "$get_block" -v block=MINPAR $slha_file)
tb=$(echo "$minpar_block" | awk -e "$get_entry" -v entry=3)

hmix_block=$(awk -e "$get_block" -v block=HMIX $slha_file)
m3=$(echo "$hmix_block" | awk -e "$get_entry" -v entry=22)
l1=$(echo "$hmix_block" | awk -e "$get_entry" -v entry=31)
l2=$(echo "$hmix_block" | awk -e "$get_entry" -v entry=32)
l3=$(echo "$hmix_block" | awk -e "$get_entry" -v entry=33)
l4=$(echo "$hmix_block" | awk -e "$get_entry" -v entry=34)
l5="0"

ZH_block=$(awk -e "$get_block" -v block=SCALARMIX $slha_file)
ZH11=$(echo "$ZH_block" | awk -e "$get_entry2" -v entry1=1 -v entry2=1)
alphaH=$(arccos $ZH11)

# convert to HDECAY convention
l1=$(prod $l1 2)
l2=$(prod $l2 2)

# convert to FORTRAN convention
mh=$(tofortran $mh)
mH=$(tofortran $mH)
mA=$(tofortran $mA)
mC=$(tofortran $mC)
tb=$(tofortran $tb)
m3=$(tofortran $m3)
alphaH="${alphaH}D0"
l1="${l1}D0"
l2="${l2}D0"
l3=$(tofortran $l3)
l4=$(tofortran $l4)
l5="${l5}D0"

sed -e "s/@TanBeta@/$tb/" \
    -e "s/@M122@/$m3/" \
    -e "s/@AlphaH@/$alphaH/" \
    -e "s/@MH1@/$mh/" \
    -e "s/@MH2@/$mH/" \
    -e "s/@MA@/$mA/" \
    -e "s/@MHp@/$mC/" \
    -e "s/@Lambda1@/$l1/" \
    -e "s/@Lambda2@/$l2/" \
    -e "s/@Lambda3@/$l3/" \
    -e "s/@Lambda4@/$l4/" \
    -e "s/@Lambda5@/$l5/" \
    < $template > $hdecay_file

# cat $hdecay_file

./run $hdecay_file
